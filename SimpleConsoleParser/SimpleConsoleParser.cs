﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SimpleConsoleParser
{
    /// <summary>
    /// Base for a new Parser
    /// </summary>
    public class SimpleConsoleParserBase
    {
        //Register of all Commands
        Dictionary<ConsoleCommandAttribute, MethodInfo> methods = new Dictionary<ConsoleCommandAttribute, MethodInfo>();

        public SimpleConsoleParserBase() : base()
        {
            Type t = GetType();
            //Get all defined Commands
            foreach (var m in t.GetMethods())
            {
                var attr = m.GetCustomAttributes(typeof(ConsoleCommandAttribute), true);
                foreach (ConsoleCommandAttribute cattr in attr)
                {
                    //Add Command to Register
                    AddCommand(cattr, m);
                }
            }
        }
        private void AddCommand(ConsoleCommandAttribute attribute, MethodInfo method)
        {
            if (!attribute.MethodMatches(method))
                throw new Exception("Method doesn't match Command");

            methods.Add(attribute, method);
        }

        protected virtual void UnknownCommand(string line)
        {

        }

        /// <summary>
        /// Try Parsing Command Line
        /// </summary>
        /// <param name="line"></param>
        public void parseCommandLine(string line)
        {
            bool matchingCommand = false;
            foreach (var cmethod in methods)
            {
                if (cmethod.Key.Matches(line))
                {
                    matchingCommand = true;
                    callMethod(cmethod.Key, cmethod.Value, line);
                    break;
                }
            }
            if (!matchingCommand)
                UnknownCommand(line);
        }
        private void callMethod(ConsoleCommandAttribute description, MethodInfo method, string line)
        {
            object[] callParameter = description.getParameters(line);
            method.Invoke(this, callParameter);
        }
    }

    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class ConsoleCommandAttribute : Attribute
    {
        public readonly Regex syntaxPattern;
        private string _syntax;
        private int _paramCount;

        /// <summary>
        /// Define Syntax for Command
        /// </summary>
        /// <param name="syntax"></param>
        public ConsoleCommandAttribute(string syntax) : base()
        {
            _syntax = syntax;
            _paramCount = 0;
            string[] parts = syntax.Split(' ');
            string fullRegex = "";
            foreach (string s in parts)
            {
                if (matchParameter(s))
                {
                    fullRegex += s + " ";
                    _paramCount++;
                }
                else if (matchSelection(s))
                {
                    string naked = s.Replace("(", "").Replace(")", "");
                    string[] options = naked.Split('/');
                    fullRegex += "(" + string.Join('|', options) + ") ";
                    _paramCount++;
                }
                else if (matchOption(s))
                {
                    string optionalArgument = s.Replace("-", "");
                    fullRegex += "(-" + optionalArgument + ")* *";
                    _paramCount++;
                }
                else
                {
                    fullRegex += "(" + s + ") ";
                }
            }

            syntaxPattern = new Regex(fullRegex.Trim(), RegexOptions.Singleline);
        }

        public bool Matches(string input)
        {
            return syntaxPattern.IsMatch(input);
        }

        public bool MethodMatches(MethodInfo info)
        {
            if (_paramCount != info.GetParameters().Count())
                return false;

            int paramIndex = 0;
            ParameterInfo[] allParams = info.GetParameters();

            foreach (string syntaxParameter in _syntax.Split(' '))
            {
                if (matchSelection(syntaxParameter) || matchParameter(syntaxParameter))
                {
                    if (allParams[paramIndex].ParameterType != typeof(string))
                        return false;
                    paramIndex++;
                }
                else if (matchOption(syntaxParameter))
                {
                    if (allParams[paramIndex].ParameterType != typeof(bool))
                        return false;
                    paramIndex++;
                }
            }

            return true;
        }

        public object[] getParameters(string input)
        {
            string[] inputParts = input.Split(' ');
            string[] syntaxParts = _syntax.Split(' ');

            List<object> parameters = new List<object>();

            int inputIndex = 0;

            for (int i = 0; i < syntaxParts.Length; i++)
            {
                string s = syntaxParts[i];
                if (matchParameter(s) || matchSelection(s))
                {
                    string para = inputParts[inputIndex];
                    parameters.Add(para);
                    inputIndex++;
                }
                else if (matchOption(s))
                {
                    Dictionary<string, bool> allOptions = new Dictionary<string, bool>();
                    do
                    {
                        s = syntaxParts[i];
                        allOptions.Add(s,false);
                        i++;
                    }
                    while (matchOption(s) && i < syntaxParts.Length);

                    if (inputIndex < inputParts.Length)
                    {
                        int inputOffset = 0;
                        string iOp = inputParts[inputIndex];
                        while (matchOption(iOp) && inputIndex + inputOffset < inputParts.Length)
                        {
                            string parsed = inputParts[inputIndex + inputOffset];
                            if(allOptions.ContainsKey(parsed))
                            {
                                allOptions[parsed] = true;
                            }
                            inputOffset++;
                        }
                    }

                    foreach (var op in allOptions)
                        parameters.Add(op.Value);
                }
                else
                    inputIndex++;
            }
            return parameters.ToArray();
        }

        private bool matchSelection(string input)
        {
            return Regex.IsMatch(input, "(\\(([a-zA-Z0-9]+|(([a-zA-Z0-9]+\\/)+[a-zA-Z0-9]+))\\))");
        }
        private bool matchParameter(string input)
        {
            return Regex.IsMatch(input, "<[a-zA-Z0-9]+>");
        }
        private bool matchOption(string input)
        {
            return Regex.IsMatch(input, "-[a-zA-Z0-9]");
        }
    }
}
